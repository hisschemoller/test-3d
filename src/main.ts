import MainScene from '@app/mainscene';
import setup from '@app/app';
import scene from '@projects/test/scene';

setup(scene as unknown as MainScene);
